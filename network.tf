resource "aws_internet_gateway" "us-east-2-igw" {
    vpc_id = "${aws_vpc.us-east-2.id}"
    tags = {
        Name = "prod-igw-us-east-2"
    }
}



resource "aws_route_table" "prod-east-crt" {
    vpc_id = "${aws_vpc.us-east-2.id}"

    route {
        //associated subnet can reach everywhere
        cidr_block = "0.0.0.0/0"         //CRT uses this IGW to reach internet
        gateway_id = "${aws_internet_gateway.us-east-2-igw.id}"
    }

    tags = {
        Name = "prod-east-crt"
    }
}






resource "aws_route_table_association" "prod-crta-pub-0"{
        subnet_id = "${aws_subnet.us-east-2a.id}"
        route_table_id = "${aws_route_table.prod-east-crt.id}"
}




resource "aws_security_group" "allow-http" {
        vpc_id = "${aws_vpc.us-east-2.id}"

        egress {
                from_port = 0
                to_port = 0
                protocol = -1
                cidr_blocks = ["0.0.0.0/0"]
        }

        ingress {
                from_port = 80
                to_port = 80
                protocol = "tcp"
                cidr_blocks = ["0.0.0.0/0"]
        }

        tags = {
                Name = "allow-http"
        }
}

resource "aws_security_group" "drop-all" {
	vpc_id = "${aws_vpc.us-east-2.id}"
	
	egress {
		from_port = 0
		to_port = 0
		protocol = -1
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	ingress {
		from_port = 0
		to_port = 0
		protocol = -1
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	tags = {
		Name = "drop-all"
	}
}
