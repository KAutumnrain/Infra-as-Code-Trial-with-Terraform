resource "aws_instance" "prod-us-e2a-testing" {
  ami           = "${data.aws_ami.amzn-linux-latest.id}"
  instance_type = "t2.micro"
  availability_zone = "us-east-2a"
  subnet_id = "${aws_subnet.us-east-2a.id}"
  vpc_security_group_ids = ["${aws_security_group.allow-http.id}"]
  tags = {
    Name = "prod-us-e2a-testing"
  }
}


resource "aws_instance" "dev-us-e2b-testing" {
	ami = "${data.aws_ami.amzn-linux-latest.id}"
	instance_type = "t2.micro"
	availability_zone = "us-east-2b"
	subnet_id = "${aws_subnet.us-east-2b.id}"
	vpc_security_group_ids = ["${aws_security_group.drop-all.id}"]	
	tags = {
	Name = "dev-us-e2b-testing"
	}
}


resource "aws_db_subnet_group" "db-demogrp" {
	name = "db-demogrp"
	subnet_ids = [aws_subnet.us-east-2a.id, aws_subnet.us-east-2b.id]	
	tags = {
		Name = "Demo DB Subnet Group"
	}

}

resource "aws_db_instance" "dev-us-db0-testing" {
	allocated_storage = 20
	engine = "mysql"
	engine_version = "8.0"
	instance_class = "db.t2.micro"
	name = "demo_db"
	multi_az = true
	username = var.AWS_DB_USERNAME
	password = var.AWS_DB_PASSWD
	skip_final_snapshot = true
	delete_automated_backups = true
	storage_type = "gp2"
	backup_retention_period = 0
	db_subnet_group_name = "${aws_db_subnet_group.db-demogrp.id}"
	tags = {
	Name = "dev-us-db0-testing"
	}

}





data "aws_ami" "amzn-linux-latest" {
	most_recent = true
	owners = ["amazon"] 
	
	filter {
		name = "name"
		values = ["amzn2-ami-kernel-*"]
	}

	filter {
		name = "architecture"
		values = ["x86_64"]
	}

}
