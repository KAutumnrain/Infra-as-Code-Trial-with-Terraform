variable "name" {
	default = "KAutumnrain's Magic Terraform Deployment!"
}

variable AWS_ACCESS_KEY_ID {
	type = string
}

variable AWS_DEFAULT_REGION {
	type = string
}

variable AWS_SECRET_ACCESS_KEY {
	type = string
}

variable AWS_DB_USERNAME {
	type = string
}

variable AWS_DB_PASSWD {
	type = string
}


