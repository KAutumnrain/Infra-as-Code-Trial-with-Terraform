resource "aws_vpc" "us-east-2" {
        cidr_block = "10.0.0.0/16"
        instance_tenancy = "default"

        tags = {
                Name = "us-east-2"
        }
}


resource "aws_subnet" "us-east-2a" {
        vpc_id = "${aws_vpc.us-east-2.id}"
        cidr_block = "10.0.1.0/24"
        map_public_ip_on_launch = "true"
	availability_zone = "us-east-2a"

        tags = {
                Name = "us-east-2a"
        }
}



resource "aws_subnet" "us-east-2b" {
        vpc_id = "${aws_vpc.us-east-2.id}"
        cidr_block = "10.0.2.0/24"
        map_public_ip_on_launch = "false"
	availability_zone = "us-east-2b"
        tags = {
                Name = "us-east-2b"
        }
}




