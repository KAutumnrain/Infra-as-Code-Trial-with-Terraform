## Description
Learning AWS, Terraform, Pipelines, Infrastructure-as-Code, and some tentative extras; the fun way!
This is part 1 of a tentative two repositories that allow me to automate AWS deployments.

## What this will entail
Ideally I'd like this project to contain the following:
- A series of terraform files to configure
- [x] One VPC 
- [x] One Internet Gateway
- [x] Two subnets in ~~differing regions~~ (The spec I am following did not specify different regions)
- [x] Two EC2 instances, one in each subnet
- [x] One load balancer
- [ ] Three CPU usage alarms with cloudwatch subscribed to a preconfigured SNS topic
- [x] One Amazon RDS instance configured for MySQL
        (one would likely want to split it off using `terraform state rm [tentativeDBname]` 
        if this deployment was more permanent)
- [x] A *.yml file to describe a fairly basic deployment pipeline (Allowing destruction of infra just for the sake of keeping AWS free-tier sorta free)

- [ ] A python script to build said terraform files

## Some notes
- The terraform files in this repository use protected and masked environment labels to authenticate to AWS
- There will be another repository at some point for configuring machines themselves using Ansible, as well
as a variant of this repo using pulumi.
- There is no one dedicated `main.tf` file for debug reasons, as I'm building and testing the terraform configuration (shown below)


## Terraform Dev Style
![image.png](./readme_assets/image.png)
